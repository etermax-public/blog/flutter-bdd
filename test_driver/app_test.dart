import 'dart:async';
import 'package:flutter_gherkin/flutter_gherkin.dart';
// ignore: depend_on_referenced_packages
import 'package:gherkin/gherkin.dart';

import 'steps/given_counter_value_is.dart';
import 'steps/then_i_see_the_value.dart';

Future<void> main() {
  const counterBaseUrlName = 'COUNTER_BASE_URL';
  var baseUrlDefine =
      "$counterBaseUrlName=${String.fromEnvironment(counterBaseUrlName)}";
  final config = FlutterTestConfiguration()
    ..dartDefineArgs = [baseUrlDefine]
    ..features = [RegExp('features/*.*.feature')]
    ..reporters = [
      StdoutReporter(),
    ]
    ..stepDefinitions = [
      GivenCounterValueIs(),
      ThenISeeTheValue(),
    ]
    ..restartAppBetweenScenarios = true
    ..targetAppPath = "test_driver/app.dart"
    ..logFlutterProcessOutput = true;

  return GherkinRunner().execute(config);
}
