import 'package:flutter/widgets.dart';
import 'package:flutter_bdd/my_app.dart';
// ignore: depend_on_referenced_packages
import 'package:flutter_driver/driver_extension.dart';

void main() {
  // This line enables the extension
  enableFlutterDriverExtension();
  runApp(const MyApp());
}
