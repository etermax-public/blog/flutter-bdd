// ignore_for_file: depend_on_referenced_packages
import 'package:gherkin/gherkin.dart';
import 'package:http/http.dart';

class GivenCounterValueIs extends Given1<int> {
  @override
  RegExp get pattern => RegExp(r'counter value is {int}');

  @override
  Future<void> executeStep(int counterValue) async =>
      await Future.microtask(() {
        var client = Client();
        client.post(
            Uri.parse(
                "${String.fromEnvironment('COUNTER_BASE_URL')}/__admin/mappings"),
            body: "{\"request\" : "
                "{\"method\" : \"GET\", \"url\" : \"/counter\" }"
                ",\"response\":{\"status\":200,\"body\": \"$counterValue\"}"
                ",\"priority\": 1}");
      });
}
