// ignore_for_file: depend_on_referenced_packages

import 'package:gherkin/gherkin.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:flutter_gherkin/flutter_gherkin.dart';

class ThenISeeTheValue extends Then1WithWorld<int, FlutterWorld> {
  @override
  RegExp get pattern => RegExp(r'I see the value {int}');

  @override
  Future<void> executeStep(int value) async {
    var finder = ByText(value.toString());
    var valueIsFound = await FlutterDriverUtils.isPresent(world.driver, finder,
        timeout: Duration(seconds: 30));
    expect(valueIsFound, true);
    return Future.value();
  }
}
