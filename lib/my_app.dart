import 'package:flutter/material.dart';
import 'package:flutter_bdd/configuration_service.dart';
import 'package:flutter_bdd/http_counter_service.dart';
import 'package:flutter_bdd/my_home_page.dart';
import 'package:flutter_bdd/my_home_page_view_model.dart';
import 'package:http/http.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(
        title: 'Flutter Demo Home Page',
        viewModel: MyHomePageViewModel(HttpCounterService(
          Client(),
          ConfigurationService(),
        )),
      ),
    );
  }
}
