import 'package:flutter/foundation.dart';
import 'package:flutter_bdd/counter_service.dart';

class MyHomePageViewModel {
  final CounterService _counterService;
  final ValueNotifier<int> _counter;

  MyHomePageViewModel(this._counterService) : _counter = ValueNotifier(0);

  ValueNotifier<int> get counterValue => _counter;

  void initialize() =>
      _counterService.getValue().then((value) => _counter.value = value);
}
