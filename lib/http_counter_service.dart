import 'package:flutter_bdd/configuration_service.dart';
import 'package:flutter_bdd/counter_service.dart';
import 'package:http/http.dart';

class HttpCounterService implements CounterService {
  final Client _client;
  final ConfigurationService _configurationService;
  HttpCounterService(
    this._client,
    this._configurationService,
  );

  @override
  Future<int> getValue() {
    return _client
        .get(Uri.parse("${_configurationService.apiBaseAddress}/counter"))
        .then((counterValue) => int.parse(counterValue.body));
  }
}
