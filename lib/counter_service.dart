abstract class CounterService {
  Future<int> getValue();
}
