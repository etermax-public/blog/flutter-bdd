Feature: Shared Counter

    Scenario: Show initial counter value
        Given counter value is 10
        Given I restart the app
        Then I see the value 10
