import 'package:fake_async/fake_async.dart';
import 'package:flutter_bdd/counter_service.dart';
import 'package:flutter_bdd/my_home_page_view_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'my_home_page_view_model_test.mocks.dart';

@GenerateNiceMocks([MockSpec<CounterService>()])
void main() {
  late CounterService counterService;
  late MyHomePageViewModel myHomePageViewModel;

  setUp(() {
    counterService = MockCounterService();
    myHomePageViewModel = MyHomePageViewModel(counterService);
  });

  test('initially counter value is zero', () {
    expect(myHomePageViewModel.counterValue.value, equals(0));
  });

  test('get counter value on initialize', () {
    myHomePageViewModel.initialize();
    verify(counterService.getValue()).called(equals(1));
  });

  test('update counter value', () {
    var newCounterValue = 10;
    var counterValueUpdated = false;
    when(counterService.getValue())
        .thenAnswer((_) => Future.value(newCounterValue));
    fakeAsync((async) {
      myHomePageViewModel.initialize();
      myHomePageViewModel.counterValue
          .addListener(() => counterValueUpdated = true);
      async.flushMicrotasks();
    });
    expect(counterValueUpdated, equals(true));
    expect(myHomePageViewModel.counterValue.value, equals(newCounterValue));
  });
}
