import 'dart:io';

import 'package:flutter_bdd/configuration_service.dart';
import 'package:flutter_bdd/http_counter_service.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'http_counter_service_test.mocks.dart';

@GenerateNiceMocks([
  MockSpec<Client>(),
  MockSpec<ConfigurationService>(),
])
void main() {
  late Client httpClient;
  late ConfigurationService configurationService;
  late HttpCounterService httpCounterService;
  const baseApiAddress = "http://localhost:9091";

  setUp(() {
    httpClient = MockClient();
    configurationService = MockConfigurationService();
    httpCounterService = HttpCounterService(httpClient, configurationService);
    when(configurationService.apiBaseAddress).thenReturn(baseApiAddress);
  });

  test('return counter value', () async {
    when(httpClient.get(Uri.parse("$baseApiAddress/counter")))
        .thenAnswer((_) => Future.value(Response("10", HttpStatus.ok)));
    var counterValue = await httpCounterService.getValue();
    expect(counterValue, equals(10));
  });
}
