import 'package:flutter/material.dart';
import 'package:flutter_bdd/my_home_page.dart';
import 'package:flutter_bdd/my_home_page_view_model.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'my_home_page_test.mocks.dart';

@GenerateNiceMocks([MockSpec<MyHomePageViewModel>()])
void main() {
  late MyHomePageViewModel myHomePageViewModel;
  var counterValue = ValueNotifier(0);

  setUp(() {
    myHomePageViewModel = MockMyHomePageViewModel();
    when(myHomePageViewModel.counterValue).thenReturn(counterValue);
  });

  testWidgets('initialize', (tester) async {
    _givenMyHomePage(tester, myHomePageViewModel);
    verify(myHomePageViewModel.initialize()).called(1);
  });
  testWidgets('get initial value', (tester) async {
    await _givenMyHomePage(tester, myHomePageViewModel);
    verify(myHomePageViewModel.counterValue).called(1);
    expect(find.text('0'), findsOneWidget);
  });
  testWidgets('update value', (tester) async {
    await _givenMyHomePage(tester, myHomePageViewModel);
    counterValue.value = 10;
    await tester.pump();
    expect(find.text('10'), findsOneWidget);
  });
}

Future<void> _givenMyHomePage(
    WidgetTester tester, MyHomePageViewModel myHomePageViewModel) {
  return tester.pumpWidget(MaterialApp(
      home: MyHomePage(
    title: '',
    viewModel: myHomePageViewModel,
  )));
}
